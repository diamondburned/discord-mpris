module github.com/diamondburned/discord-mpris

go 1.12

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/davecgh/go-spew v1.1.1
	github.com/dustin/go-humanize v1.0.0
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/manifoldco/promptui v0.3.2
)
