package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/bwmarrin/discordgo"
)

var token = os.Getenv("TOKEN")

func main() {
	if token == "" {
		panic("loser")
	}

	d, err := discordgo.New(token)
	if err != nil {
		panic(err)
	}

	d.StateEnabled = false
	d.SyncEvents = false

	must(d.Open())

	defer d.Close()

	cmd := exec.Command(
		"playerctl",
		"-f", "{{status}}\033{{title}}\033{{artist}}",
		"-F", "metadata",
	)

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}

	must(cmd.Start())

	defer func() {
		cmd.Process.Signal(os.Interrupt)
		cmd.Process.Release()
	}()

	emptyUpdated := false

	scanner := bufio.NewScanner(stdout)
	for scanner.Scan() {
		fields := strings.Split(scanner.Text(), "\033")
		if len(fields) != 3 {
			fmt.Println(fields)
			continue
		}

		var (
			status = fields[0]
			title  = fields[1]
			artist = fields[2]
		)

		if status != "Playing" {
			if !emptyUpdated {
				must(d.UpdateStatus(0, ""))
				emptyUpdated = true
			}
		} else {
			emptyUpdated = false
			must(d.UpdateStatusComplex(discordgo.UpdateStatusData{
				Game: &discordgo.Game{
					Name:    "music",
					Details: title,
					State:   artist,
					Type:    discordgo.GameTypeListening,
				},
			}))
		}
	}

	must(scanner.Err())
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
