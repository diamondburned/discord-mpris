# discord-mpris

Application to push current song to Discord using Dbus MPRIS

## Dependencies

- `playerctl`

## Instructions

```sh
go build
TOKEN=here ./discord-mpris
```

